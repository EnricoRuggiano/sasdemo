
/* include macros */
%let DEMO_PATH='/folders/myfolders/demo_mc1_logic.sas';
%include &DEMO_PATH;
options mlogic;

/* ************************************** */
/* DEMO - LEVEL 1 */
/* ************************************** */

/* create input tables */ 
%m_input_table(Impiegato, Periodo, Stipendio, ITA_input_table);
%m_input_table(Employeer, Period, Salary, ENG_input_table);

%macro HR_Salary_Calcs_lev_1(
  tblIn,      /* input table */
  colID,      /* ID attrb name */
  colYYYYMM,  /* Date attrib name */
  colSalary,  /* Salary attrib name */
  tblOut);    /* output table */

  %m_output_table(
    &colID, 
    &colYYYYMM, 
    &colSalary, 
    &tblIn, 
    &tblOut);

%mend HR_Salary_Calcs_lev_1;

%HR_Salary_Calcs_lev_1(
  ITA_input_table, 
  Impiegato, 
  Periodo, 
  Stipendio, 
  ITA_output_table);

%HR_Salary_Calcs_lev_1(
  ENG_input_table, 
  Employeer, 
  Period, 
  Salary, 
  ENG_output_table);

/* ************************************** */
/* DEMO - LEVEL 2 */
/* ************************************** */

%macro HR_Salary_Calcs_lev_2(
  tblIn,                      /* input table */
  colID,                      /* ID attrb name */
  colYYYYMM,                  /* Date attrib name */
  colSalary,                  /* Salary attrib name */
  tblOut,                     /* output table */
  startDate = &TWO_YEARS_AGO, /* Start date */
  endDate = TODAY(),          /* End date */
  cumulativeVars = YTD#LTD,  /* Cumulative vars */
  differenceVars = 1#3#6#12);  /* Difference vars  */

  %m_output_table(
    &colID, 
    &colYYYYMM, 
    &colSalary, 
    &tblIn, 
    &tblOut, 
    START_DATE = &startDate, 
    END_DATE = &endDate, 
    cumulativeVars = &cumulativeVars, 
    differenceVars = &differenceVars);

%mend HR_Salary_Calcs_lev_2;

/* default cumulative and difference */
%HR_Salary_Calcs_lev_2(
  ITA_input_table, 
  Impiegato, 
  Periodo, 
  Stipendio, 
  ITA_output_table, 
  startDate = '1feb2018'd, 
  endDate = '4jun2019'd);

/* default date and empty cumulative */
%HR_Salary_Calcs_lev_2(
  ENG_input_table, 
  Employeer, 
  Period, 
  Salary, 
  ENG_output_table, 
  cumulativeVars = , 
  differenceVars = 1#2#3#4#5);

/* ************************************** */
/* DEMO - LEVEL CHALLENGE */
/* ************************************** */

/* macros for params */
%let DATA_PATH = /folders/myfolders/demo_mc1_data/;
%let TWO_YEARS_AGO = INTNX('year', TODAY(), -2);

%macro HR_Salary_Calcs_lev_3(
  colID,          /* ID attrb name */
  colSalary,      /* Salary attrib name */
  libn,           /* Lib name */
  tblnPattern,    /* Table pattern name */
  colPeriod);     /* Date attrib name */
 
 %let TABLE = HR_3;
 
 /* create an unique table with all table present in libn */
  data &TABLE._Input;
  set &libn..&tblnPattern.: indsname=table_name;
  
  /*  parse the table name and add the date attrib */
  dateString = scan(table_name, 2, '_');
  Year  = substr(dateString, 1, 4);
  Month = substr(dateString, 5, 2);
  
  /* debug */
  *putlog table_name "ID: " &colID;
  
  &colPeriod = mdy(Month, 1, Year);
  
  format &colPeriod date9.;
  drop Year Month;
  drop dateString; /* debug parsing is correct */
  run;
 
 /* calculate the output_table */
 %m_output_table(
    &colID, 
    &colPeriod, 
    &colSalary, 
    &TABLE._Input, 
    &TABLE._Output);

%mend HR_Salary_Calcs_lev_3;


/* example code */

%m_external_table( 
  Impiegato, 
  Periodo, 
  Stipendio,  
  &DATA_PATH,
  ITA,
  01jan2018,
  03mar2020);

%HR_Salary_Calcs_lev_3(
  Impiegato,
  Stipendio,
  ITA,
  Impiegato,
  Data);
