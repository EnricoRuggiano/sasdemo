/* ***************************** */
/* demo exercise for pg1 and pg2 */
/* ***************************** */

%let BASE_ID 1000;
%let BASE_SALARY 1000;
%let NUM_EMPLOYEERS 4;
%let MODIFIER_SALARY rand('Integer', 70, 150) / 100; /* -30% +50% of base salary*/
%let NOT_MISSING_ROW rand('Integer', 1, 100) > 20; /* 80% is not missing*/
%let MONTH_PASSED = INTCK('month', t1.Date, t2.Date);

/*	Input Table - create the table */
data demo_input_table;
	end_date = TODAY();
	start_date = INTNX('year', TODAY(), -2);
	step_date = start_date;
	
	do i=1 to &NUM_EMPLOYEERS;
		Id = rand('Integer', &BASE_ID, &BASE_ID + 10000);
		do while (step_date < end_date);
			Date = step_date;
			Salary = &BASE_SALARY * &MODIFIER_SALARY;
			
			/* radomly drop this row*/
			if(&NOT_MISSING_ROW) then output;
			
			/* increment by one month*/
			step_date = INTNX('month', step_date, 1);
		end;
		step_date = start_date;
	end;
	
	/* format styles */
	format Date MMYYD.;
	format Salary dollar.;
	
	/* clean from useless stuff */
	drop i start_date step_date end_date;
run;

/* PARSE Table - add integers Month and Year attributes to group records later*/
data demo_parsed_table;
	set demo_input_table;
	Month = month(Date);
	Year  = year(Date);
run;

/* BUG FIX - check in output table deleted records.Date < 2019 are 0*/
/*proc sql;
	delete from demo_parsed_table
	where Date < INTNX('year', TODAY(), -1);
quit;
*/

/* IDS Table - table of distinct id of the employers */
proc sql;
	create table demo_ids_table as
	select distinct Id
	from work.demo_parsed_table;
quit;

/* ZERO Table - a table with 0 default value of missing attribs */ 
data demo_zero_table;
	set demo_ids_table;
	end_year = year(Today());
	start_year = year(INTNX('year', TODAY(), -2));
	
	do Year=start_year to end_year;
		do Month= 1 to 12;
			Salary = 0;
			Diff_1M = 0;
			Diff_3M = 0;
			Diff_6M = 0;
			Diff_12M = 0;
			Date = mdy(Month, 1, Year);
			
			/* break the cycle if default record.date > today */
			if(mdy(Month, 1, Year) < TODAY()) then output;
		end;
		Month = 1;
	end;
	drop start_year end_year;
run;

/* SORTED Table - sort the data by year and month */
proc sort data=demo_parsed_table out=demo_sorted_table;
	by Id Year Month;
run;

/* TEMP Table - Union Zero and Sorted Table. Missing values are 0 now */
data demo_tmp_table;
	merge demo_zero_table demo_sorted_table;
	by Id Year Month ;
		
	/* Life Time Salary */
	if (First.Id = 1) then LTD_Salary=0;
	LTD_Salary + Salary;
	
	/* Year Time Salary */
	if (First.Year = 1) then YTD_Salary=0;
	YTD_Salary + Salary;
	
	/* Diff - shortcut using lag */
	sal_lag1  =  lag(Salary);
	sal_lag3  =  lag3(Salary);
	sal_lag6  =  lag6(Salary);
	sal_lag12 = lag12(Salary);
	
	if(sal_lag1  < 1) then sal_lag1  = 0;
	if(sal_lag3  < 1) then sal_lag3  = 0;
	if(sal_lag6  < 1) then sal_lag6  = 0;
	if(sal_lag12 < 1) then sal_lag12 = 0;
	
	Diff_1M = Salary - sal_lag1;
	Diff_3M  = Salary - sal_lag3;
	Diff_6M  = Salary - sal_lag6;
	Diff_12M = Salary - sal_lag12;
	
	format LTD_Salary YTD_Salary dollar.;
	format Diff_1M Diff_3M Diff_6M Diff_12M dollar.;	
	drop sal_lag1 sal_lag3 sal_lag6 sal_lag12 ;
run;

/* ****************************************************** */
/* SQL Tables to get the differences of salaries per Month */
/* ****************************************************** */

/* 12 Months */
proc sql;
	create table demo_12_month as
	select t1.Id, 
		t2.Date as Date, 
		t1.Date as Past_DATE, 
		t2.Salary - t1.Salary as Diff_12M,
		t2.Salary as Curr_Salary, 
		t1.Salary as Past_Salary
	from work.demo_tmp_table t1, work.demo_tmp_table t2
	where &MONTH_PASSED = 12  and t1.id = t2.id;
quit;

/* 6 Months */
proc sql;
	create table demo_6_month as
	select t1.Id, 
		t2.Date as Date, 
		t1.Date as Past_DATE, 
		t2.Salary - t1.Salary as Diff_6M,
		t2.Salary as Curr_Salary, 
		t1.Salary as Past_Salary
	from work.demo_tmp_table t1, work.demo_tmp_table t2
	where &MONTH_PASSED = 6  and t1.id = t2.id;
quit;

/* 3 Months */
proc sql;
	create table demo_3_month as
	select t1.Id, 
		t2.Date as Date, 
		t1.Date as Past_DATE, 
		t2.Salary - t1.Salary as Diff_3M,
		t2.Salary as Curr_Salary, 
		t1.Salary as Past_Salary
	from work.demo_tmp_table t1, work.demo_tmp_table t2
	where &MONTH_PASSED = 3  and t1.id = t2.id;
quit;

/* 1 Month */
proc sql;
	create table demo_1_month as
	select t1.Id, 
		t2.Date as Date, 
		t1.Date as Past_DATE, 
		t2.Salary - t1.Salary as Diff_1M,
		t2.Salary as Curr_Salary, 
		t1.Salary as Past_Salary
	from work.demo_tmp_table t1, work.demo_tmp_table t2
	where &MONTH_PASSED = 1  and t1.id = t2.id;
quit;

/* ************* */
/* Output Table	 */
/* ************* */

/* lag + diff version */
data demo_output_table;
	set demo_tmp_table;
run;

/* sql version */
data old_demo_output_table;
	merge demo_tmp_table(drop=Diff_1M Diff_3M Diff_6M Diff_12M)
		demo_1_month
		demo_3_month 
		demo_6_month 
		demo_12_month;
	by Id Date;
	keep Id Date Salary LTD_Salary YTD_Salary Diff_1M Diff_3M Diff_6M Diff_12M; 
	format YTD_Salary LTD_Salary dollar.;
	format Diff_1M Diff_3M Diff_6M Diff_12M dollar.;	
run;