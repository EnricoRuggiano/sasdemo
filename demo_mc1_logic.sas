/* MACRO DEBUGS */
options mlogic;

/* MACRO GENERALS */
%let TWO_YEARS_AGO = INTNX('year', TODAY(), -2);

/************************************************************/
/* MACRO TABLES */

/* ***************************** */
/* MACRO - INIT TABLE            */
/* ***************************** */
%macro m_input_table(
  ATTR_ID, 
  ATTR_DATE, 
  ATTR_SAL, 
  OUT_TABLE, 
  START_DATE = &TWO_YEARS_AGO, 
  END_DATE = TODAY(), 
  NUM_EMPL = 4,
  DROP_DATE = 0);
  
  /* MACROS FOR INPUT TABLE */
  %let BASE_ID = 1234;
  %let BASE_SALARY = 1000;
  %let GET_ID = rand('Integer', &BASE_ID, &BASE_ID + 10000);
  %let MODIFIER_SALARY = rand('Integer', 70, 150) / 100; /* -30% +50% of base salary*/
  %let NOT_MISSING_ROW = rand('Integer', 1, 100) > 20;  /* 80% is not missing*/
  
  /* create input table */
  data &OUT_TABLE;
    end_date   = &END_DATE;
    start_date = &START_DATE;
    step_date  = start_date;

    do i = 1 to &NUM_EMPL;
    
      /* WorkAround for HR_3 -> id are not random but sequential */
      &ATTR_ID=i;
        
      do while (step_date < end_date);
        &ATTR_DATE = step_date;
        &ATTR_SAL  = &BASE_SALARY * &MODIFIER_SALARY;
        putlog "ID = " &ATTR_ID;
        
        /* radomly drop this row*/
        if(&NOT_MISSING_ROW) then
          output;
    
        /* increment by one month*/
        step_date = INTNX('month', step_date, 1);
      end;
      step_date = start_date;
    end;
    format &ATTR_DATE MMYYD.;
    format &ATTR_SAL dollar.;
    drop i start_date step_date end_date;
    
    /* drop Date column */
    %if &DROP_DATE %then drop &ATTR_DATE;
  run;

%mend m_input_table;

/* ***************************** */
/* MACRO - OUTPUT TABLE          */
/* ***************************** */
%macro m_output_table(
  ATTR_ID, 
  ATTR_DATE, 
  ATTR_SAL, 
  IN_TABLE, 
  OUT_TABLE, 
  START_DATE = &TWO_YEARS_AGO, 
  END_DATE = TODAY(), 
  cumulativeVars = YTD#LTD, 
  differenceVars = 1#3#6#12,
  NUM_EMPL = 4);
  
  /* PARSE Table - add integers Month and Year attributes to group records later*/
  data demo_parsed_table;
    set &IN_TABLE;
    Month = month(&ATTR_DATE);
    Year  = year(&ATTR_DATE);
    where &ATTR_DATE between &START_DATE and &END_DATE;
  run;

  /* IDS Table - table of distinct id of the employers */
  proc sql;
    create table demo_ids_table as select distinct &ATTR_ID
      from work.demo_parsed_table 
      where &ATTR_DATE between &START_DATE and &END_DATE;
  quit;

  /* ZERO Table - a table with 0 default value of missing attribs */
  data demo_zero_table;
    
    /* consider only specified number of ids */
    set demo_ids_table(OBS=&NUM_EMPL);
    
    start_year  = year(&START_DATE);
    end_year    = year(&END_DATE);
    start_month = month(&START_DATE);
    end_month   = month(&END_DATE);
    drop start_year end_year start_month end_month;

    do Year=start_year to end_year;
      start_month = 1;
      end_month   = 12;

      if(Year = start_year) then
        start_month = month(&START_DATE);

      if(Year = end_year) then
        end_month = month(&END_DATE);

      do Month = start_month to end_month;
        &ATTR_SAL = 0;

        /* init Diff_xM to 0 */
        %init_difference;

        /* init Date */
        &ATTR_DATE = mdy(Month, 1, Year);

        /* consider only date before last date */
        if(mdy(Month, 1, Year) <= &END_DATE) then
          output;
      end;
    end;
  run;

  /* FILTERED Table - consider only Ids present in Parsed Table*/
  proc sql;
    create table demo_filtered_table as 
      select * 
      from demo_ids_table(OBS=&NUM_EMPL) natural left join demo_parsed_table;
  quit;

  /* SORTED Table - sort the data by year and month */
  proc sort data = demo_filtered_table out = demo_sorted_table;
    by &ATTR_ID Year Month;
  run;

  /* OUTPUT Table - Union Zero and Sorted Table. Missing values are 0 now */
  data &OUT_TABLE;
    merge demo_zero_table demo_sorted_table;
    by &ATTR_ID Year Month;

    /* Calculate the Diff_xM */
    %parse_difference;

    /* Calculate LTD and YTD */
    %parse_cumulative;
  
    drop Month Year;
  run;

%mend m_output_table;

/************************************************************/
/* MACRO UTILS */

/* ************************************************** */
/* MACRO CUMULATIVE - it parses and calculate YTD#LTD */
/* ************************************************** */
%macro parse_cumulative;
  %let pos = 0;
  %let len = 0;
  %let sep = #;

  %do word=1 %to %sysfunc(countw(&cumulativeVars, &sep));
    %syscall scan(cumulativeVars, word, pos, len, sep);
    %let TOKEN = %substr(&cumulativeVars, &pos, &len);

    %if &TOKEN=LTD %then
      %do;

        /* LTD - code */
        if (First.&ATTR_ID = 1) then LTD_&ATTR_SAL = 0;
        LTD_&ATTR_SAL + &ATTR_SAL;
        format LTD_&ATTR_SAL dollar.;
      %end;

    %if &TOKEN=YTD %then
      %do;

        /* YTD - code */
        if (First.Year = 1) then YTD_&ATTR_SAL = 0;
        YTD_&ATTR_SAL + &ATTR_SAL;
        format YTD_&ATTR_SAL dollar.;
      %end;
  %end;
%mend parse_cumulative;

/* **************************************************** */
/* MACRO DIFFERENCE - it parses and calculates 1#3#6#12 */
/* **************************************************** */
%macro parse_difference;
  %let pos = 0;
  %let len = 0;
  %let sep = #;

  %do word=1 %to %sysfunc(countw(&differenceVars, &sep));
    %syscall scan(differenceVars, word, pos, len, sep);
    %let NUM = %substr(&differenceVars, &pos, &len);

    /* DIFFERENCE CODE */
    
    %if &NUM > 1 %then
      %do;
        sal_lag&NUM = lag&NUM.(&ATTR_SAL);
        if(sal_lag&NUM < 1) then sal_lag&NUM = 0;
        Diff_&NUM.M = &ATTR_SAL - sal_lag&NUM;
        drop sal_lag&NUM;
        format Diff_&NUM.M dollar.;
      %end;
    %else
      %do;
        sal_lag = lag(&ATTR_SAL);
        if(sal_lag < 1) then sal_lag=0;
        Diff_1M = &ATTR_SAL - sal_lag;
        drop sal_lag;
        format Diff_1M dollar.;
      %end;
  %end;
%mend parse_difference;

/* **************************************************** */
/* MACRO INIT DIFFERENCE - it inits Diff_xM to 0        */
/* **************************************************** */
%macro init_difference;
  %let pos = 0;
  %let len = 0;
  %let sep = #;

  %do word=1 %to %sysfunc(countw(&differenceVars, &sep));
    %syscall scan(differenceVars, word, pos, len, sep);
    %let NUM = %substr(&differenceVars, &pos, &len);
    Diff_&NUM.M = 0;
    format Diff_&NUM.M dollar.;
  %end;
%mend init_difference;

/* ***************************************************** */
/* MACRO EXTERNAL TABLE - it inits tables for one period */
/* ***************************************************** */

%macro m_external_table(
  ATTR_ID,      /* ID attrib name */
  ATTR_DATE,    /* Date attrib name */
  ATTR_SAL,     /* Salary attrib name */
  DATA_PATH,    /* Path for the lin */
  LIBNAME,      /* Lib name */
  START_DATE,   /* must be date9. format */
  END_DATE,     /* must be date9. format */
  NUM_EMPL = 4);
  
  %let step_date = %sysfunc(inputn(&START_DATE, date9.));
  %let stop_date = %sysfunc(inputn(&END_DATE, date9.));
  
  /* create my library*/
  libname &LIBNAME "&DATA_PATH";
  
  /* create a table for each month */
  %DO %WHILE (&step_date le &stop_date);
    
    %let YEAR   = %sysfunc(year(&step_date));
    %let MONTH  = %sysfunc(month(&step_date));
    
    /* work around: concat 0 to solve one digit months */
    %if &MONTH < 10 %then %let MONTH = 0&MONTH;
    
    %let TABLE_NAME  = &ATTR_ID._&YEAR.&MONTH.;
    %let nxt_step_date = %sysfunc(INTNX(month, &step_date, 1));
    
    /* create single month table*/
    %m_input_table(
      &ATTR_ID, 
      DONT_CARE, 
      &ATTR_SAL, 
      &LIBNAME..&TABLE_NAME, 
      START_DATE = &step_date, 
      END_DATE = &nxt_step_date, 
      NUM_EMPL = &NUM_EMPL,
      DROP_DATE = 1);
    
    /* Increment by one month*/
    %let step_date = %sysfunc(INTNX(month, &step_date, 1));
  %end;  
%mend m_external_table;

/************************************************************/
/* Some Debug stuff */
/*
%m_input_table(
  Impiegato, 
  Periodo, Soldi, 
  demo_input_table, 
  START_DATE='12jan1998'd, 
  END_DATE='1jan2000'd, 
  NUM_EMPL=4);*/

/*%m_output_table(
  Impiegato, 
  Periodo, 
  Soldi, 
  demo_input_table, 
  demo_output_table, 
  START_DATE = '1feb1998'd, 
  END_DATE= '1jun1999'd,
  cumulativeVars = ,
  differenceVars = );*/
 
 /*%m_external_table( 
  Impiegato, 
  Periodo, 
  Stipendio,  
  &DATA_PATH,
  ITA,
  01dec2018,
  03mar2020);*/